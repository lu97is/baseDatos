CREATE FUNCTION public.get_actor(q_first_name character varying, q_last_name character varying) RETURNS TABLE(t_actor_id integer, t_first_name character varying, t_last_name character varying, t_last_update timestamp without time zone)
    LANGUAGE plpgsql
    AS $$
BEGIN
 RETURN QUERY SELECT
 actor_id,
 first_name,
 last_name,
 last_update
 FROM
 actor
 WHERE
 first_name ilike q_first_name or last_name ilike q_last_name;
END; $$;


CREATE FUNCTION public.get_categories() RETURNS TABLE(q_category_id integer, q_name character varying, q_last_update timestamp without time zone)
    LANGUAGE plpgsql
    AS $$
begin
	return query select
	category_id, name, last_update
	from category
	where active = true;
end; $$;

CREATE FUNCTION public.insert_category(q_name character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    INSERT INTO category (name, last_update) VALUES (q_name, current_timestamp);
END
$$;

CREATE FUNCTION public.update_category(q_id integer, q_name character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    update category set name = q_name, last_update = current_timestamp where category_id = q_id;
END
$$;

CREATE FUNCTION public.delete_category(q_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    update category set active = false where category_id = q_id;
END;
$$;

CREATE FUNCTION public.insert_rental(q_inventory_id integer, q_customer_id smallint, q_return_date timestamp without time zone, q_staff_id smallint) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    INSERT INTO rental (rental_date,inventory_id, customer_id, return_date, staff_id, last_update, active)
     VALUES (current_timestamp, q_inventory_id, q_customer_id, q_return_date, q_staff_id, current_timestamp, true);
END
$$;

CREATE FUNCTION public.get_movies() RETURNS TABLE(q_title character varying, q_film_id integer)
    LANGUAGE plpgsql
    AS $$
begin
	return query select
	title,
	film_id
	from film;
end; $$;

CREATE FUNCTION public.update_rental(q_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    update rental set active = false where rental_id = q_id;
END;
$$;

CREATE FUNCTION public.get_rental_customer(q_customer_id integer) RETURNS TABLE(t_rental_id integer, t_rental_date timestamp without time zone, t_tittle character varying, t_store_id smallint, t_return_date timestamp without time zone, t_reffer character varying)
    LANGUAGE plpgsql
    AS $$
begin
	return query select r.rental_id, r.rental_date ,f.title, i.store_id, r.return_date ,s.first_name
from rental as r
inner join inventory as i on r.inventory_id = i.inventory_id
inner join film as f on f.film_id = i.film_id
inner join staff as s on s.staff_id = r.staff_id 
where r.customer_id = q_customer_id and r.active = true;
end; $$;

CREATE FUNCTION public.get_staff() RETURNS TABLE(q_first_name character varying, q_staff_id integer)
    LANGUAGE plpgsql
    AS $$
begin
	return query select
	first_name,
	staff_id
	from staff;
end; $$;


CREATE FUNCTION public.get_store() RETURNS TABLE(q_store_id integer)
    LANGUAGE plpgsql
    AS $$
begin
	return query select
	store_id
	from store;
end; $$;

CREATE FUNCTION public.get_filmsl() RETURNS TABLE(t_store_id smallint, t_title character varying, t_quantity bigint)
    LANGUAGE plpgsql
    AS $$
BEGIN
 RETURN QUERY SELECT
	i.store_id,
	f.title ,
	count(f.title)
from inventory as i
inner join film as f 
on f.film_id = i.film_id
inner join store as s
on s.store_id = i.store_id
group by i.store_id, f.title;
END; $$;

CREATE FUNCTION public.get_films_filter(q_patter character varying) RETURNS TABLE(t_store_id smallint, t_title character varying, t_quantity bigint)
    LANGUAGE plpgsql
    AS $$
BEGIN
 RETURN QUERY SELECT
	i.store_id,
	f.title ,
	count(f.title)
from inventory as i
inner join film as f 
on f.film_id = i.film_id
inner join store as s
on s.store_id = i.store_id
group by i.store_id, f.title
having f.title ilike q_patter;
END; $$;

CREATE FUNCTION public.get_films_filter_store(q_patter smallint) RETURNS TABLE(t_store_id smallint, t_title character varying, t_quantity bigint)
    LANGUAGE plpgsql
    AS $$
BEGIN
 RETURN QUERY SELECT
	i.store_id,
	f.title ,
	count(f.title)
from inventory as i
inner join film as f 
on f.film_id = i.film_id
inner join store as s
on s.store_id = i.store_id
group by i.store_id, f.title
having i.store_id = q_patter;
END; $$;
--
select * from actor where active = true
--
insert into actor(first_name,last_name,last_update) values(${first_name}, ${last_name}, ${last_update})
--
update actor set first_name=$1, last_name=$2, last_update=$3 where actor_id=$4
--
update actor set active = false where actor_id = $1

--
select * from city

--
select * from country

--
insert into customer(username,password,first_name,last_name,address_id,email,store_id,activebool,create_date,last_update,active)
values(${username}, ${password}, ${first_name}, ${last_name},${address_id}, ${email}, ${store_id},${activebool},${create_date},${last_update},${active})

--
select * from customer where username = $1 and password = $2

--
insert into address(address,address2,district,city_id,postal_code,phone,last_update)
values(${address},${address2},${district},${city_id},${postal_code},${phone},${last_update})
RETURNING address_id
