import { MaterializeModule } from "angular2-materialize";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UserService } from "./services/user.service";
import { Http,HttpModule } from "@angular/http";
import { AppComponent } from './app.component';
import { RegisterComponent } from './components/user/register/register.component';
import { app_routing} from './app.routing';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import 'materialize-css';
import { DashboardComponent } from './components/user/dashboard/dashboard.component';
import { RentComponent } from './components/user/dashboard/rent/rent.component';
import { CategoryComponent } from './components/user/dashboard/category/category.component';
import { ActorComponent } from './components/user/dashboard/actor/actor.component';
import { MovieComponent } from './components/user/dashboard/movie/movie.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    DashboardComponent,
    RentComponent,
    CategoryComponent,
    ActorComponent,
    MovieComponent
  ],
  imports: [
    app_routing,
    MaterializeModule,
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
