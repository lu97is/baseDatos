import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from "./components/user/register/register.component";
import { DashboardComponent } from "./components/user/dashboard/dashboard.component";
import { RentComponent } from "./components/user/dashboard/rent/rent.component";
import { ActorComponent } from "./components/user/dashboard/actor/actor.component";
import { CategoryComponent } from "./components/user/dashboard/category/category.component";
import { MovieComponent } from "./components/user/dashboard/movie/movie.component";
const app_routes: Routes = [
  { path: 'home', component: RegisterComponent },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'dashboard/rent', component:RentComponent},
  { path: 'dashboard/actor', component: ActorComponent},
  { path: 'dashboard/movie', component: MovieComponent},
  { path: 'dashboard/category', component: CategoryComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const app_routing = RouterModule.forRoot(app_routes);