import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { UserService } from "../../../services/user.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, AfterViewInit {
  address_id: any;
  @ViewChild('cityId') cityId: ElementRef;
  register: Boolean = false;
  addressReg: Boolean = false;
  countries: any[] = [];
  cities: any[] = [];
  aux: Number;
  form: FormGroup;
  city: any;
  found:Boolean;
  constructor(private _userService: UserService, private formBuilder: FormBuilder, private router:Router) {
    this.createForm();
    _userService.getCountries().subscribe(data => this.countries = data.data);
    _userService.getCities().subscribe(data => this.cities = data.data);
    this.form.valueChanges.subscribe(data => {
      this.aux = data.country;
    });
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  openRegister() {
    this.register = true;

  }

  address() {
    this.addressReg = true;

  }
  createForm() {
    this.form = this.formBuilder.group({
      country: ['', Validators.required],
      city: ['', Validators.required],
      address: ['', Validators.required],
      address2: ['', Validators.required],
      phone: ['', Validators.required],
      postal_code: ['', Validators.required],
      district: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.required],
      loginUsername: ['', Validators.required],
      loginPassword: ['', Validators.required]
    })
  }

  registerUser() {
    let dateObj = new Date();
    let month = dateObj.getUTCMonth() + 1; //months from 1-12
    let day = dateObj.getUTCDate();
    let year = dateObj.getUTCFullYear();
    let hours = dateObj.getHours();
    let minutes = dateObj.getMinutes();
    let second = dateObj.getSeconds();
    let newdate = year + "-" + month + "-" + day + ' ' + hours + ':' + minutes + ':' + second;
    console.log(newdate)
    const address = {
      country_id: this.form.get('country').value,
      city_id: this.cityId.nativeElement.value,
      address: this.form.get('address').value,
      address2: this.form.get('address2').value,
      phone: this.form.get('phone').value,
      postal_code: this.form.get('postal_code').value,
      district: this.form.get('district').value,
      last_update : newdate
    }
   
    this._userService.postAddress(address).subscribe(data => {
      const user = {
        username: this.form.get('username').value,
        password: this.form.get('password').value,
        first_name: this.form.get('first_name').value,
        last_name: this.form.get('last_name').value,
        email: this.form.get('email').value,
        address_id : data.message,
        activebool: true,
        create_date: newdate,
        last_update: newdate,
        active: 1,
        store_id: 1
      }
      this._userService.postCustomer(user).subscribe(data => {

        this._userService.login(user).subscribe(data =>{
          if(data.data.length == 0){
            this.found = true;
          }else{
            this._userService.storeData(data.data[0].username, data.data[0].password)
            this.router.navigate(['/dashboard']);
          }
        })
     })
     
    })
}

  login() {
    const user = {
      username: this.form.get('loginUsername').value,
      password: this.form.get('loginPassword').value
    }
    this._userService.login(user).subscribe(data =>{
      console.log(data.data)
      if(data.data.length == 0){
        this.found = true;
      }else{
        this._userService.storeData(data.data[0].username, data.data[0].password)
        this.router.navigate(['/dashboard']);
      }
    })
  }
}
