import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UserService } from "../../../../services/user.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-rent',
  templateUrl: './rent.component.html',
  styleUrls: ['./rent.component.scss']
})
export class RentComponent implements OnInit {
  rentals: any[] = [];
  optionnS = 1;
  staff: any[] = [];
  store: any[] = [];
  movies: any[] = [];
  form: FormGroup;
  constructor(private router: Router, private _user: UserService, private fb: FormBuilder) {
    _user.getRentals(localStorage.getItem('Id')).subscribe(data => {
      this.rentals = data.message;
      console.log(data.message)
    })
    _user.getStaff().subscribe(data => {
      console.log(data.message)
      this.staff = data.message
    })
    _user.getStore().subscribe(data => {
      console.log(data.message)
      this.store = data.message
    })
    _user.getFilms().subscribe(data => {
      this.movies = data.message
      console.log(this.movies)
    })
    this.createForm();
  }

  ngOnInit() {
  }
  reloadRentals() {
    this._user.getRentals(localStorage.getItem('Id')).subscribe(data => {
      this.rentals = data.message;
    })
  }

  createForm() {
    this.form = this.fb.group({
      returnDate: ['', Validators.required],
      movie: ['', Validators.required],
      staff: ['', Validators.required],
      store: ['', Validators.required]
    });
  }
  goTo(e) {
    switch (e) {
      case 'rent':
        this.router.navigate(['/dashboard/' + e]);
      case 'category':
        this.router.navigate(['/dashboard/' + e]);
      case 'actor':
        this.router.navigate(['/dashboard/' + e]);
      case 'movie':
        this.router.navigate(['/dashboard/' + e]);
    }
  }
  delete(e) {
    this._user.deleteRental(e).subscribe(data => {
      this.reloadRentals();
    })
  }

  sendRent() {
    const user = {
      customer_id: localStorage.getItem('Id'),
      inventory_id: this.form.get('movie').value,
      staff_id: this.form.get('staff').value,
      return_date: this.form.get('returnDate').value,
    }
    console.log(user)
    this._user.postRental(user).subscribe(data => {
      this.reloadRentals();
      this.optionnS = 1;
    })
  }

}
