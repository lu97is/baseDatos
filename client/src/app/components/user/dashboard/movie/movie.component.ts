import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UserService } from "../../../../services/user.service";
@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {
  movie:any [] = []
  constructor(private router:Router, private _user:UserService) {
    _user.getMovie('').subscribe(data => {
      console.log(data)
      this.movie = data.message
    })
   }

  ngOnInit() {
  }
  goTo(e){
    switch(e){
      case 'rent':
        this.router.navigate(['/dashboard/' + e]);
      case 'category':
        this.router.navigate(['/dashboard/' + e]);
      case 'actor':
        this.router.navigate(['/dashboard/' + e]);
      case 'movie':
        this.router.navigate(['/dashboard/' + e]);
    }
  }

}
