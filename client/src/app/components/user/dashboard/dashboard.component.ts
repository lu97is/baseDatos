import { Component, OnInit } from '@angular/core';
import { UserService} from "../../../services/user.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  user:any [] = [];
  constructor(private _user:UserService, private router:Router) {
    const user = {
      username: localStorage.getItem('user'),
      password: localStorage.getItem('password')
    }
    _user.login(user).subscribe(data=> {
      this.user = data.data[0]
      localStorage.setItem('Id', data.data[0].customer_id);
    })
   }

  ngOnInit() {

  }

  goTo(e){
    switch(e){
      case 'rent':
        this.router.navigate(['/dashboard/' + e]);
      case 'category':
        this.router.navigate(['/dashboard/' + e]);
      case 'actor':
        this.router.navigate(['/dashboard/' + e]);
      case 'movie':
        this.router.navigate(['/dashboard/' + e]);
    }
  }

  delete(e){
    console.log(e)
  }

}
