import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { UserService } from "../../../../services/user.service";
@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.scss']
})
export class ActorComponent implements OnInit {
  searchQ: boolean;
  actors:any [] = [];
  editSID: any;
  optionS:Number = 1;
  categoryName:String;
  form:FormGroup;
  editS:Boolean = false;
  constructor(private router:Router, private _user:UserService, private fb:FormBuilder) { 
    this.createForm();

  }

  ngOnInit() {
    this.reloadActors();
  }

  goTo(e){
    switch(e){
      case 'rent':
        this.router.navigate(['/dashboard/' + e]);
      case 'category':
        this.router.navigate(['/dashboard/' + e]);
      case 'actor':
        this.router.navigate(['/dashboard/' + e]);
      case 'movie':
        this.router.navigate(['/dashboard/' + e]);
    }
  }

  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      last_name: ['', Validators.required]
    });
  }

  reloadActors(){
    this._user.getActors().subscribe(data => {
      this.actors = data.data;
      console.log(this.actors)
    })
  }

  edit(e){
    this.editS = true;
    this.editSID = e;
  }

  sendEdit(){
    let dateObj = new Date();
    let month = dateObj.getUTCMonth() + 1; //months from 1-12
    let day = dateObj.getUTCDate();
    let year = dateObj.getUTCFullYear();
    let hours = dateObj.getHours();
    let minutes = dateObj.getMinutes();
    let second = dateObj.getSeconds();
    let newdate = year + "-" + month + "-" + day + ' ' + hours + ':' + minutes + ':' + second;
    const actor = {
      first_name: this.form.get('name').value,
      last_name: this.form.get('last_name').value,
      last_update : newdate
    }
    this._user.putActor(actor, this.editSID).subscribe(data =>{
      this.reloadActors();
      this.optionS = 1;
      this.searchQ = false;
    }) 
  }

  addActor(){
    let dateObj = new Date();
    let month = dateObj.getUTCMonth() + 1; //months from 1-12
    let day = dateObj.getUTCDate();
    let year = dateObj.getUTCFullYear();
    let hours = dateObj.getHours();
    let minutes = dateObj.getMinutes();
    let second = dateObj.getSeconds();
    let newdate = year + "-" + month + "-" + day + ' ' + hours + ':' + minutes + ':' + second;
    const actor = {
      first_name: this.form.get('name').value,
      last_name: this.form.get('last_name').value,
      last_update: newdate
    }
    this._user.postActor(actor).subscribe(data => {
      this.reloadActors();
      this.optionS = 1;
    })
  }

  delete(e){
    this._user.deleteActor(e).subscribe(data => this.reloadActors())
  }

  searchActor(){
    let first_name;
    let last_name;
    if(this.form.get('name').value == ''){
      first_name = "''"

    }else{
      first_name = this.form.get('name').value
    }
    if(this.form.get('last_name').value == ''){
      last_name = "''"

    }else{
      last_name =this.form.get('last_name').value
    }
    this._user.getActor(first_name,last_name).subscribe(data =>{
      console.log(data.message)
      this.searchQ = true;
      this.actors = data.message;
    })
  }
}
