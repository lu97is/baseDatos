import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UserService } from "../../../../services/user.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  editSID: any;
  optionS:Number = 1;
  categories:any [] = [];
  categoryName:String;
  form:FormGroup;
  editS:Boolean = false;
  constructor(private router:Router, private _user:UserService, private fb: FormBuilder) {
    this.createForm();
   }

  ngOnInit() {
    this.reloadCategories();
  }
  goTo(e){
    switch(e){
      case 'rent':
        this.router.navigate(['/dashboard/' + e]);
      case 'category':
        this.router.navigate(['/dashboard/' + e]);
      case 'actor':
        this.router.navigate(['/dashboard/' + e]);
      case 'movie':
        this.router.navigate(['/dashboard/' + e]);
    }
  }

  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
    });
  }

  reloadCategories(){
    this._user.getCategories().subscribe(data => {
      this.categories = data.message;
      console.log(this.categories)
    })
  }

  addCategory(){
    console.log(this.form.get('name').value)
    const category = {
      name: this.form.get('name').value
    } 
    this._user.postCategory(category).subscribe(data => {
      this.reloadCategories();
      this.optionS = 1
    })
  }

  delete(e){
    this._user.deleteCategory(e).subscribe(data => this.reloadCategories())
  }

  edit(e){
    this.editS = true;
    this.editSID = e;
  }

  sendEdit(){
    const category = {
      name: this.form.get('name').value
    }
    this._user.putCategory(category, this.editSID).subscribe(data =>{
      this.reloadCategories();
      this.optionS = 1;
    }) 
  }

}
