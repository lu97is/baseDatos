import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { isDevMode } from '@angular/core';
import { map } from "rxjs/operators";
@Injectable()
export class UserService {
  domain: String = 'http://localhost:8080/';
  user:String;
  password:String;
  constructor(private http: Http) {
  }
  //register
  getProduct() {
    return this.http.get(this.domain + 'api/actors').pipe(map(res => res.json()));
  }
  getCities() {
    return this.http.get(this.domain + 'api/cities').pipe(map(res => res.json()));
  }
  getCountries(){
    return this.http.get(this.domain + 'api/countries').pipe(map(res => res.json()));
  }
  postCustomer(user) {
    return this.http.post(this.domain + 'api/customer', user).pipe(map(res => res.json()));
  }
  postAddress(add) {
    return this.http.post(this.domain + 'api/address', add).pipe(map(res => res.json()));
  }
  login(user){
    return this.http.post(this.domain + 'api/login', user).pipe(map(res => res.json()));
  }
  storeData(user, password) {
    localStorage.setItem('user', user);
    localStorage.setItem('password', password);
    this.user = user;
    this.password = password;
  }

  //actor
  getActors(){
    return this.http.get(this.domain + 'api/actors').pipe(map(res => res.json()));
  }
  postActor(actor){
    return this.http.post(this.domain + 'api/actor', actor).pipe(map(res => res.json()));
  }
  putActor(actor,id){
    return this.http.put(this.domain + 'api/actor/' + id, actor).pipe(map(res => res.json()));
  }
  deleteActor(id){
    return this.http.delete(this.domain + 'api/actor/' + id).pipe(map(res => res.json()));
  }
  getActor(first_name?, last_name?){
    return this.http.get(this.domain + 'api/actor/' + first_name + '/' + last_name).pipe(map(res => res.json()));
  }

  //categories
  getCategories(){
    return this.http.get(this.domain + 'api/categories').pipe(map(res => res.json()));
  }
  postCategory(category){
    return this.http.post(this.domain + 'api/categories', category).pipe(map(res => res.json()));
  }
  putCategory(category, id){
    return this.http.put(this.domain + 'api/categories/' + id, category).pipe(map(res => res.json()));
  }
  deleteCategory(id){
    return this.http.delete(this.domain + 'api/categories/' + id).pipe(map(res => res.json()));
  }

  //rental
  getRentals(id){
    return this.http.get(this.domain + 'api/rentals/' + id).pipe(map(res => res.json()));
  }
  postRental(rental){
    return this.http.post(this.domain + 'api/rental', rental).pipe(map(res => res.json()));
  }
  deleteRental(id){
    return this.http.delete(this.domain + 'api/rental/' + id).pipe(map(res => res.json()));
  }

  getStore(){
    return this.http.get(this.domain + 'api/store').pipe(map(res => res.json()));
  }
  
  getStaff(){
    return this.http.get(this.domain + 'api/staff').pipe(map(res => res.json()));
  }

  //movies
  getMovies(){
    return this.http.get(this.domain + 'api/movies').pipe(map(res => res.json()));
  }
  getMovie(filter){
    return this.http.get(this.domain + 'api/movies/' + filter).pipe(map(res => res.json()));
  }
  getMoviesStore(filter){
    return this.http.get(this.domain + 'api/movies-store/' + filter).pipe(map(res => res.json()));
  }
  getFilms(){
    return this.http.get(this.domain + 'api/films').pipe(map(res =>res.json()));
  }
}