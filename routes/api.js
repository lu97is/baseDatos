'use strict'
const promise = require('bluebird');
const options = {
    promiseLib: promise
};

const pgp = require('pg-promise')(options);
const connectionString = 'postgres://postgres:eagle1997@localhost:5432/DvdRental'
const db = pgp(connectionString);

module.exports = (router) => {
    //actor
    router.get('/actors', (req, res, next) => {
        db.query('select * from actor where active = true')
            .then(function (data) {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data

                    });
            })
            .catch(function (err) {
                return next(err);
            });
    })
    router.post('/actor', (req, res, next) => {
        db.none('insert into actor(first_name,last_name,last_update)' +
                'values(${first_name}, ${last_name}, ${last_update})',
                req.body)
            .then(function () {
                res.status(200)
                    .json({
                        status: 'success',
                        message: 'Inserted one puppy'
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    });
    router.put('/actor/:id', (req, res, next) => {
        db.none('update actor set first_name=$1, last_name=$2, last_update=$3 where actor_id=$4', [req.body.first_name, req.body.last_name, req.body.last_update, parseInt(req.params.id)])
            .then(function () {
                res.status(200)
                    .json({
                        status: 'success',
                        message: 'Updated actor'
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    })
    router.delete('/actor/:id', (req, res, next) => {
        db.result('update actor set active = false where actor_id = $1', req.params.id)
            .then(function (result) {
                res.status(200)
                    .json({
                        status: 'success',
                        message: `Removed ${result.rowCount} puppy`
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    })

    router.get('/actor/:first_name?/:last_name?', (req,res,next)=>{
        db.func('get_actor', ['%' + req.params.first_name + '%', '%' + req.params.last_name + '%'])
        .then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err);
        })
    })


    //extras
    router.get('/cities', (req, res, next) => {
        db.query('select * from city')
            .then(function (data) {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    });
    router.get('/countries', (req, res, next) => {
        db.query('select * from country')
            .then(function (data) {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    });    
    router.post('/customer', (req, res, next) => {
        db.none('insert into customer(username,password,first_name,last_name,address_id,email,store_id,activebool,create_date,last_update,active)' +
            'values(${username}, ${password}, ${first_name}, ${last_name},${address_id}, ${email}, ${store_id},${activebool},${create_date},${last_update},${active})',
            req.body).then(() => {
            res.status(200).json({
                status: 'success',
                message: 'User register'
            })
        }).catch((err) => {
            return next(err);
        })
    })
    router.post('/login', (req,res,next)=>{
        db.query('select * from customer where username = $1 and password = $2', [req.body.username, req.body.password])
        .then((data) => {
            res.status(200).json({
                status: 'success',
                data: data
            })
        }).catch((err) => {
            return next(err);
        })
    })
    router.post('/address', (req, res, next) => {
        db.query('insert into address(address,address2,district,city_id,postal_code,phone,last_update)' +
                'values(${address},${address2},${district},${city_id},${postal_code},${phone},${last_update})' +
                'RETURNING address_id', req.body)
            .then((data) => {
                res.status(200).json({
                    status: 'success',
                    message: data[0].address_id
                })
            }).catch((err) => {
                return next(err);
            })
    })

    
    //categories
    router.get('/categories', (req,res,next)=>{
        db.func('get_categories').then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err);
        })
    })
    router.post('/categories', (req,res,next)=>{
        db.func('insert_category', req.body.name).then(function(data){
            res.status(200).json({
                success: true,
                message: 'Category inserted'
            })
        }).catch(function(err){
            return next(err)
        })
    })
    router.put('/categories/:id', (req,res,next)=>{
        db.func('update_category',[ req.params.id, req.body.name]).then(function(data){
            res.status(200).json({
                success: true,
                message: 'Category updated'
            })
        }).catch(function(err){
            return next(err)
        })
    })
    router.delete('/categories/:id',(req,res,next)=>{
        db.func('delete_category', req.params.id).then(function(data){
            res.status(200).json({
                success: true,
                message: 'Deleted'
            })
        }).catch(function(err){
            return next(err)
        })
    })

    //rental
    router.post('/rental', (req,res,next)=>{
        db.func('insert_rental', 
        [req.body.inventory_id,
        req.body.customer_id,
        req.body.return_date,
        req.body.staff_id]).then(function(data){
            res.status(200).json({
                success: true,
                message: 'Rental inserted'
            })
        }).catch(function(err){
            return next(err);
        })
    })
    router.get('/films', (req,res,next)=>{
        db.func('get_movies').then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err)
        })
    })
    router.delete('/rental/:id', (req,res,next)=>{
        db.func('update_rental', req.params.id).then(function(data){
            res.status(200).json({
                success: true,
                message: 'Deleted'
            })
        }).catch(function(err){
            return next(err);
        })
    })
    router.get('/rentals/:customer', (req,res,next)=>{
        db.func('get_rental_customer', req.params.customer).then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err)
        })
    })

    router.get('/staff', (req,res,next)=>{
        db.func('get_staff').then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err)
        })
    })

    router.get('/store', (req,res,next)=>{
        db.func('get_store').then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err)
        })
    })

    //movies
    router.get('/movies',(req,res,next)=>{
        db.func('get_filmsl').then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err);
        })
    });
    router.get('/movies/:filter', (req,res,next)=>{
        db.func('get_films_filter', '%' + req.params.filter + '%').then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err)
        })
    });

    router.get('/movies-store/:filter?', (req,res,next)=>{
        db.func('get_films_filter_store', req.params.filter).then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err)
        })
    });
    return router;
}